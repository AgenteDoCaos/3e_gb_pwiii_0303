import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImoveisLitoraisComponent } from './imoveis-litorais.component';



@NgModule({
  declarations: [
    ImoveisLitoraisComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ImoveisLitoraisModule { }
