import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImoveisLitoraisComponent } from './imoveis-litorais.component';

describe('ImoveisLitoraisComponent', () => {
  let component: ImoveisLitoraisComponent;
  let fixture: ComponentFixture<ImoveisLitoraisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImoveisLitoraisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImoveisLitoraisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
