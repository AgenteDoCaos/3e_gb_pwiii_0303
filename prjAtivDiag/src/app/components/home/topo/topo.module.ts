import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopoComponent } from './topo.component';



@NgModule({
  declarations: [
    TopoComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TopoModule { }
