import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroComponent } from './filtro.component';



@NgModule({
  declarations: [
    FiltroComponent
  ],
  imports: [
    CommonModule
  ]
})
export class FiltroModule { }
