import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImoveisUrbanosComponent } from './imoveis-urbanos.component';



@NgModule({
  declarations: [
    ImoveisUrbanosComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ImoveisUrbanosModule { }
