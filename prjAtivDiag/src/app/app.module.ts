import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeModule } from './components/home/home.module';
import { DestaquesModule } from './components/home/destaques/destaques.module';
import { FiltroModule } from './components/home/filtro/filtro.module';
import { TopoModule } from './components/home/topo/topo.module';
import { FeedbackModule } from './components/home/feedback/feedback.module';
import { CadImovelModule } from './components/cad-imovel/cad-imovel.module';
import { ImoveisRuraisModule } from './components/imoveis-rurais/imoveis-rurais.module';
import { ImoveisUrbanosModule } from './components/imoveis-urbanos/imoveis-urbanos.module';

@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    DestaquesModule,
    FiltroModule,
    TopoModule,
    FeedbackModule,
    CadImovelModule,
    ImoveisRuraisModule,
    ImoveisUrbanosModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

